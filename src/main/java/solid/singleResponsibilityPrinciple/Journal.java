package solid.singleResponsibilityPrinciple;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;


//A class should only have a single responsibility, that is, only changes to one
//part of the software's specification should be able to affect the specification of the class.
class Journal {
    private final List<String> entries = new ArrayList<String>();
    public static int count = 0;

    public void addEntry(String text) {
        entries.add("" + (++count) + ": " + text);
    }
    public void removeEntry(int index) {
        entries.remove(index);
        --count;
    }

    @Override
    public String toString() {
        return String.join(System.lineSeparator(), entries);
    }

}
class Persistence {
    public void saveToFile(Journal jurnal, String filename, boolean override) throws FileNotFoundException {
        if(override || new File(filename).exists()) {
            try (PrintStream out = new PrintStream(filename)){
                out.println(jurnal.toString());
            }
        }
    }
}
class Main {
    public static void main(String[] args) throws IOException {
        Journal jurnal = new Journal();
        jurnal.addEntry("First input");
        jurnal.addEntry("Second input");
        System.out.println(jurnal);

        Persistence p = new Persistence();
        String path = "journal.txt";
        p.saveToFile(jurnal, path, true);
        Runtime.getRuntime().exec("notepad.exe " + path);
    }
}
