package solid.interfaceSegregationPrinciple;

//Many client-specific interfaces are better than one general-purpose interface.
class Document{

}
interface Printer{
    void print(Document document);
}
interface Scanner{
    void scan(Document document);
}
interface Fax{
    void fax(Document document);
}

class JustAPrinter implements Printer{

    @Override
    public void print(Document document) {
        System.out.println("Printing");
    }
}

class PrinterWithScanner implements Printer, Scanner{

    @Override
    public void print(Document document) {
        System.out.println("Printing");
    }

    @Override
    public void scan(Document document) {
        System.out.println("Scanning");
    }
}

class MultiMachine implements Printer, Scanner, Fax {

    @Override
    public void print(Document document) {
        System.out.println("Printing");
    }

    @Override
    public void scan(Document document) {
        System.out.println("Scanning");
    }

    @Override
    public void fax(Document document) {
        System.out.println("Sending fax");
    }
}
