package solid.liskovSubstitutionPrinciple;
// Objects in a program should be replaceable with instances of their
// subtypes without altering the correctness of that program.
class Ractangle{
    protected int width;
    protected int height;

    public Ractangle() {
    }

    public Ractangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Ractangle[" +
                "width=" + width +
                ", height=" + height +
                ']';
    }

    public int getArea(){
        return width * height;
    }
}
class Square extends Ractangle{
    public Square(int side) {
        width = height = side;
    }

    public Square() {
    }

    @Override
    public void setWidth(int width) {
        super.setWidth(width);
        if(super.getHeight() != width) {
            super.setHeight(width);
        }
    }

    @Override
    public void setHeight(int height) {
        super.setHeight(height);

        if(super.getWidth() != height)
            super.setWidth(height);
    }

    @Override
    public int getArea() {
        return super.getArea();
    }

}

 class Demo{
    static void useIt(Ractangle rc){
        int width = rc.getWidth();
        rc.setHeight(10);
        System.out.println("Expected area: " + (width * 10) + ", got: " + rc.getArea());
    }

     public static void main(String[] args) {
         Ractangle ractangle = new Square();
         useIt(ractangle);
     }
}
