package solid.openClosePrinciple;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;


//Software entities ... should be open for extension, but closed for modification.
enum Color {
    RED,
    GREEN,
    BLUE
}
enum Size {
    SMALL,
    MEDIUM,
    LARGE,
    EXTRA_LARGE,
    EXTRA_EXTRA_LARGE
}
class Product {
    public String name;
    public Color color;
    public Size size;

    public Product(String name, Color color, Size size) {
        this.name = name;
        this.color = color;
        this.size = size;
    }

    @Override
    public String toString() {
        return "Product[" +
                "name='" + name + '\'' +
                ", color=" + color +
                ", size=" + size +
                ']';
    }
}

interface Specification<T> {
    boolean isSatisfied(T item);
}
interface Filter<T> {
    Stream<T> filter(List<T> items, Specification<T> specification);
}

class ProductColorSpecification implements Specification<Product>{
    private Color color;

    public ProductColorSpecification(Color color) {
        this.color = color;
    }

    @Override
    public boolean isSatisfied(Product item) {
        return item.color == color;
    }
}

class ProductSizeSpecification implements Specification<Product>{
    private Size size;

    public ProductSizeSpecification(Size size) {
        this.size = size;
    }


    @Override
    public boolean isSatisfied(Product item) {
        return item.size == size;
    }
}
class ProductColorFilter implements Filter<Product>{

    @Override
    public Stream<Product> filter(List<Product> items, Specification<Product> specification) {
        return items.stream().filter(specification::isSatisfied);
    }
}
class ProductSizeFilter implements Filter<Product>{

    @Override
    public Stream<Product> filter(List<Product> items, Specification<Product> specification) {
        return items.stream().filter(specification::isSatisfied);
    }
}
class Main {
    public static void main(String[] args) {
        Product apple = new Product("Apple", Color.GREEN, Size.SMALL);
        Product tree = new Product("Tree", Color.GREEN, Size.LARGE);
        Product house = new Product("House", Color.BLUE, Size.EXTRA_EXTRA_LARGE);

        List<Product> products = Arrays.asList(apple, tree, house);

        ProductColorFilter productColorFilter = new ProductColorFilter();
        System.out.println("Green products (new):");
        productColorFilter.filter(products, new ProductColorSpecification(Color.GREEN));

        ProductSizeFilter productSizeFilter = new ProductSizeFilter();
        System.out.println("Small products:");
        productSizeFilter.filter(products, new ProductSizeSpecification(Size.EXTRA_EXTRA_LARGE)).forEach(p -> System.out.println(" - " + p.name + " is extra extra large"));



    }
}

