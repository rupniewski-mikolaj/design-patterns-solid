package solid.dependencyInversionPrinciple;

// High-level modules should not depend on low-level modules.
// Both should depend on abstractions

import org.javatuples.Triplet;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

enum Relationship{
    PARENT,
    CHILD,
    SIBLING;
}

class Person{
    public String name;

    public Person(String name) {
        this.name = name;
    }
}
// low-level module
class Relationships implements RelationshipBrowser{
    private List<Triplet<Person, Relationship, Person>> relations = new ArrayList<>();

    public List<Triplet<Person, Relationship, Person>> getRelations() {
        return relations;
    }

    public void addParentAndChild(Person parent, Person child){
        relations.add(new Triplet<>(parent, Relationship.PARENT, child));
        relations.add(new Triplet<>(child, Relationship.CHILD, parent));
    }

    @Override
    public List<Person> findAllChildrenOf(String name) {
        return relations.stream().filter(x -> Objects.equals(x.getValue0().name, name) &&
        x.getValue1() == Relationship.PARENT).map(Triplet::getValue2).collect(Collectors.toList());
    }
}
interface RelationshipBrowser{
    List<Person> findAllChildrenOf(String name);
}
//high-level module
class Research{
    // high-level module depends on low-level module because its injected in constructor
    public Research(Relationships relationships){
        List<Triplet<Person, Relationship, Person>> relations = relationships.getRelations();
        relations.stream().filter(x -> x.getValue0().name.equals("John") && x.getValue1() == Relationship.PARENT)
                .forEach(ch -> System.out.println("John has a chil called " + ch.getValue2().name));
    }
    // Good way
    public Research(RelationshipBrowser relationshipBrowser){
        List<Person> children = relationshipBrowser.findAllChildrenOf("John");
        for(Person child : children){
            System.out.println(child.name + " is John's child");
        }
    }

    public Research() {
    }
}
class Demo{
    public static void main(String[] args) {
        Person john = new Person("John");
        Person chris = new Person("Chris");
        Person matt = new Person("Matt");

        Relationships relationships = new Relationships();
        relationships.addParentAndChild(john,chris);
        relationships.addParentAndChild(john, matt);

        Research research = new Research(relationships);


    }
}
