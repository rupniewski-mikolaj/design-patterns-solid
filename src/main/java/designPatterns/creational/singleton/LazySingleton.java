package designPatterns.creational.singleton;


class LazySingleton{
    private static LazySingleton instance;

    private LazySingleton(){
        System.out.println("Inside LazySingleton constructor");
    }
    public static synchronized LazySingleton getInstance(){
        if(instance == null)
            instance = new LazySingleton();
        return instance;
    }
}