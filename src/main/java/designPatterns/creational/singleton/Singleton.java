package designPatterns.creational.singleton;

import javax.xml.crypto.Data;
import java.io.*;

class DatabaseConnection implements Serializable{
    private String dbUrl = "jdbc:mysql://127.0.0.1:3306/blog";
    private String dbUser = "root";
    private String dbPassword = "root";
    private String dbDriver = "com.mysql.jdbc.Driver";
    private static final DatabaseConnection INSTANCE = new DatabaseConnection();

    private DatabaseConnection(){

    }

    public static DatabaseConnection getInstance(){
        return INSTANCE;
    }

    public String getDbUrl() {
        return dbUrl;
    }

    public void setDbUrl(String dbUrl) {
        this.dbUrl = dbUrl;
    }

    public String getDbUser() {
        return dbUser;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public String getDbDriver() {
        return dbDriver;
    }

    public void setDbDriver(String dbDriver) {
        this.dbDriver = dbDriver;
    }

    @Override
    public String toString() {
        return "DatabaseConnection{" +
                "dbUrl='" + dbUrl + '\'' +
                ", dbUser='" + dbUser + '\'' +
                ", dbPassword='" + dbPassword + '\'' +
                ", dbDriver='" + dbDriver + '\'' +
                '}';
    }

    protected Object readResolve(){
        return INSTANCE;
    }
}

class Demo{

    static void saveTofile(DatabaseConnection databaseConnection, String filename) throws IOException {
        FileOutputStream fileOut = new FileOutputStream(filename);
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        try {
            out.writeObject(databaseConnection);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    static DatabaseConnection readFromFile(String filename) throws IOException {
        FileInputStream fileIn = new FileInputStream(filename);
        ObjectInputStream in = new ObjectInputStream(fileIn);
        try{
            return (DatabaseConnection) in.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static void main(String[] args) throws IOException {
        DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
        databaseConnection.setDbUser("mikolaj");

        String filename = "singleton.bin";
        saveTofile(databaseConnection,filename);
        DatabaseConnection databaseConnection1 = readFromFile(filename);

        System.out.println(databaseConnection);
        System.out.println(databaseConnection1);
    }
}
