package designPatterns.behavioral.factories;

import java.util.HashMap;
import java.util.Map;

interface Animal {
    String getAnimal();
    String makeSound();
}

class Duck implements Animal {

    @Override
    public String getAnimal() {
        return "Duck";
    }

    @Override
    public String makeSound() {
        return "Squeks";
    }
}
class Dog implements Animal {

    @Override
    public String getAnimal() {
        return "Duck";
    }

    @Override
    public String makeSound() {
        return "Woof";
    }
}
class Cat implements Animal {

    @Override
    public String getAnimal() {
        return "Cat";
    }

    @Override
    public String makeSound() {
        return "Miau";
    }
}
interface AbstractFactory<T> {
    T create(String animalType);
    T mapVerionCreate(String animalType);
}

class AnimalFactory implements AbstractFactory<Animal> {

    @Override
    public Animal create(String animalType) {
        if ("Dog".equalsIgnoreCase(animalType)) {
            return new Dog();
        } else if ("Duck".equalsIgnoreCase(animalType)) {
            return new Duck();
        } else if ("Duck".equalsIgnoreCase(animalType)) {
            return new Cat();
        }
        return null;
    }

    @Override
    public Animal mapVerionCreate(String animalType) {
        Map<String, Animal> animals = new HashMap<>();
        animals.put("Duck", new Duck());
        animals.put("Cat", new Cat());
        animals.put("Dog", new Dog());
        return animals.get(animalType);
    }
}
