package designPatterns.creational.builder;
class Item{
    public String name;
    public String category;

    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}
class ItemBuilder<SELF extends ItemBuilder>{
    protected Item item = new Item();

    public SELF withName(String name){
        item.name = name;
        return  self();
    }
    public Item build(){
        return item;
    }
    protected SELF self(){
        return (SELF) this;
    }
}
class ProductBuilder extends ItemBuilder<ProductBuilder>{
    public ProductBuilder withCategory(String category){
        item.category = category;
        return self();
    }

    @Override
    protected ProductBuilder self() {
        return this;
    }
}
class Demo{
    public static void main(String[] args) {
        ProductBuilder productBuilder = new ProductBuilder();
        Item item = productBuilder.withName("pencil").withCategory("office").build();
        System.out.println(item);

    }
}