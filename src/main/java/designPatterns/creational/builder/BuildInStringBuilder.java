package designPatterns.creational.builder;

public class BuildInStringBuilder {
    public static void main(String[] args) {
        String[] words = {"hello", "world"};
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<ul>\n");
        for(String word : words){
            stringBuilder.append(String.format("  <li>%s</li>\n", word));
        }
        stringBuilder.append("</ul>");
        System.out.println(stringBuilder);
    }
}
