package designPatterns.creational.builder;

class Person{
    public String streetAddress;
    public String postCode;
    public String city;
    public String companyName;
    public String position;
    public int annualIncome;

    @Override
    public String toString() {
        return "Person{" +
                "streetAddress='" + streetAddress + '\'' +
                ", postCode='" + postCode + '\'' +
                ", city='" + city + '\'' +
                ", companyName='" + companyName + '\'' +
                ", position='" + position + '\'' +
                ", annualIncome=" + annualIncome +
                '}';
    }
}

class PersonBuilder{
    protected Person person = new Person();

    public Person build(){
        return person;
    }
    public PersonAddressBuilder lives(){
        return new PersonAddressBuilder(person);
    }
    public PersonWorkBuilder works(){
        return new PersonWorkBuilder(person);
    }
}

class PersonAddressBuilder extends PersonBuilder{

    PersonAddressBuilder(Person person){
        this.person = person;
    }

    public PersonAddressBuilder at(String streetAddress){
        person.streetAddress = streetAddress;
        return this;
    }

    public PersonAddressBuilder withPostcode(String postcode){
        person.postCode = postcode;
        return this;
    }
    public PersonAddressBuilder in(String city){
        person.city = city;
        return this;
    }
}

class PersonWorkBuilder extends PersonBuilder{
    PersonWorkBuilder(Person person){
        this.person = person;
    }
    public PersonWorkBuilder in(String companyName){
        person.companyName = companyName;
        return this;
    }
    public PersonWorkBuilder as(String position){
        person.position = position;
        return this;
    }
    public PersonWorkBuilder earns(int annualIncome){
        person.annualIncome = annualIncome;
        return this;
    }
}
class Main{
    public static void main(String[] args) {
        PersonBuilder pb = new PersonBuilder();
        Person person = pb.lives().at("Union Square 12").withPostcode("525-121").in("London").works().in("Google").as("Developer").earns(120000).build();
        System.out.println(person);
    }
}