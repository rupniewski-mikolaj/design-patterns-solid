package designPatterns.structural.adapter;

import java.util.ArrayList;
import java.util.List;

interface Shape {

    void draw();
    void resize();
    String description();
    boolean isHide();
}
class Rectangle implements Shape{

    @Override
    public void draw() {
        System.out.println("Drawing Rectangle");
    }

    @Override
    public void resize() {
        System.out.println("Resizing Rectangle");
    }

    @Override
    public String description() {
        return "Rectangle object";
    }

    @Override
    public boolean isHide() {
        return false;
    }
}
class Circle implements Shape {

    @Override
    public void draw() {
        System.out.println("Drawing Circle");
    }

    @Override
    public void resize() {
        System.out.println("Resizing Circle");
    }

    @Override
    public String description() {
        return "Circle object";
    }

    @Override
    public boolean isHide() {
        return false;
    }
}
class Drawing {
    List<Shape> shapes;

    public Drawing(){
        shapes = new ArrayList<>();
    }

    public void addShape(Shape shape){
        shapes.add(shape);
    }

    public List<Shape> getShapes(){
        return new ArrayList<Shape>(shapes);
    }

    public void resize(){
        if(shapes.isEmpty()){
            System.out.println("There is nothing to resize");
        } else {
            shapes.forEach(Shape::resize);
        }
    }
    public void draw() {
        if (shapes.isEmpty()) {
            System.out.println("Nothing to draw!");
        } else {
            shapes.forEach(Shape::draw);
        }

    }
}
class Main {

    public static void main(String[] args) {

        System.out.println("Creating drawing of shapes...");

        Drawing drawing = new Drawing();
        drawing.addShape(new Rectangle());
        drawing.addShape(new Circle());
        System.out.println("Drawing...");
        drawing.draw();
        System.out.println("Resizing...");
        drawing.resize();

    }

}