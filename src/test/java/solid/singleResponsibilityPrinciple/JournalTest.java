package solid.singleResponsibilityPrinciple;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JournalTest {
    Journal getJournal(){
        Journal journal = new Journal();
        journal.addEntry("First input");
        journal.addEntry("Second input");
        journal.addEntry("Third input");
        journal.addEntry("Fourth input");
        return journal;
    }
    @Test
    void addToJournalTest(){
        Journal journal = new Journal();
        journal.addEntry("First input");
        assertEquals(1, Journal.count);
        journal.addEntry("Second input");
        assertEquals(2, Journal.count);

    }
    @Test
    void removeFromJournalTest(){
        Journal journal = getJournal();
        assertEquals(6, Journal.count);
        journal.removeEntry(0);
        assertEquals(5,Journal.count);
        journal.removeEntry(0);
        assertEquals(4,Journal.count);
    }

}