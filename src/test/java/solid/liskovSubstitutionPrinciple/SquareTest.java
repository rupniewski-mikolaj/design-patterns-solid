package solid.liskovSubstitutionPrinciple;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SquareTest {

    @Test
    void LiskovSubstitutionPrincipleTest(){
        Ractangle square = new Square(5);
        assertEquals(5, square.getHeight());
        assertEquals(5, square.getWidth());
        assertEquals(25, square.getArea());
        square.setHeight(10);
        assertEquals(10, square.getHeight());
        assertEquals(10, square.getWidth());
        assertEquals(100, square.getArea());
        square.setWidth(15);
        assertEquals(15, square.getHeight());
        assertEquals(15, square.getWidth());
        assertEquals(225, square.getArea());
    }
}