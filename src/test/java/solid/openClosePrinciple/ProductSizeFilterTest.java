package solid.openClosePrinciple;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ProductFilterTest {
    List<Product> getProducts() {
        Product apple = new Product("Apple", Color.GREEN, Size.SMALL);
        Product tree = new Product("Tree", Color.GREEN, Size.LARGE);
        Product house = new Product("House", Color.BLUE, Size.EXTRA_EXTRA_LARGE);
        Product ship = new Product("Ship", Color.RED, Size.EXTRA_EXTRA_LARGE);
        Product mountain = new Product("Mountain", Color.GREEN, Size.EXTRA_EXTRA_LARGE);
        Product plane = new Product("Plane", Color.RED, Size.EXTRA_EXTRA_LARGE);

        return Arrays.asList(apple, tree, house, ship, mountain,plane);
    }
    @Test
    void SizeFilterTest(){
        ProductSizeFilter productSizeFilter = new ProductSizeFilter();
        Stream<Product> productStream = productSizeFilter.filter(getProducts(), new ProductSizeSpecification(Size.EXTRA_EXTRA_LARGE));
        List<Product> products = productStream.collect(Collectors.toList());

        assertEquals(4, products.size());
        for(Product product : products){
            assertEquals(Size.EXTRA_EXTRA_LARGE, product.size);
        }
    }
    @Test
    void ColorFilterTest() {
        ProductColorFilter productColorFilter = new ProductColorFilter();
        Stream<Product> productStream = productColorFilter.filter(getProducts(), new ProductColorSpecification(Color.GREEN));
        List<Product> products = productStream.collect(Collectors.toList());

        assertEquals(3, products.size());
        for(Product product : products) {
            assertEquals(Color.GREEN, product.color);
        }
    }
}